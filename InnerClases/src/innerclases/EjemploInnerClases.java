/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package innerclases;

/**
 *
 * @author USRKAP
 */
public class EjemploInnerClases {
    
   private String carro = "Mercedes";
   
   //inner class = Una clase dentro de otra
   
   class partesCarro{
       
       private String parte1 = "bujia";
       private String parte2 = "bateria";
       
       public partesCarro(){
           
           // se accede al atributo de la clase principal
           System.out.println(EjemploInnerClases.this.carro);
           
           // se accede a los atributos porpios de la inner clase
           System.out.println(this.parte1 + " o " + parte2);
       }
   }
}

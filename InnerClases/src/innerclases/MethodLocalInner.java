/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package innerclases;

/**
 *
 * @author USRKAP
 */
public class MethodLocalInner {
    
    String marcaAuto = "Ferrari";
    static int velMaxima = 300;
    private int caballosfuerza = 9; 
 
    public void partesCarro() {
        
        final int i = 5;
        final String codMotor = "BASD-123";
 
        class InnerMetodo{
 
            private String velmaxima = "300km/s";
 
            //solo se puede acceder a los atributos final del método
            private void Resultados() {
                System.out.println(MethodLocalInner.this.marcaAuto);
                System.out.println(MethodLocalInner.velMaxima);
                System.out.println(caballosfuerza); 
 
                System.out.println(codMotor);
                System.out.println(this.velmaxima); // Ok. B o B.
            }
        }
        //se cra objeto de la clase interna y se podran acceder hasta a los private
        InnerMetodo a = new InnerMetodo();
        a.Resultados();
    }
    
}

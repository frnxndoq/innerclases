/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package innerclases;

/**
 *
 * @author USRKAP
 */
public class StaticNestedClasses {
    
    //clase estática
    static class claseEstatica{
 
        private String marcaAuto = "Jeep";
 
    }
 
    //acceder clase estática
    public void crearAnidadaStaticDesdeMetodoDeInstancia(){
 
    System.out.println(new claseEstatica().marcaAuto );
    }
 
    public static void crearAnidadaStaticDesdeMetodoDeClase(){
 
        //acceder clase estática
       claseEstatica  anidadast2 = new claseEstatica();
  } 
    
}

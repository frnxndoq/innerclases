/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package innerclases;

/**
 *
 * @author USRKAP
 */
public class AnonymousInnerClasses {
    
    //clase anónima: instancia
    public Autos auto = new Autos() {
 
        //método para la interface
        public void Marcas(String nomMarca) {
           System.out.println("Coche de marca: " + nomMarca+"!");
        }
 
    };
 
    //clase anonima: clase.
    public static Automovil automovil = new Automovil() {
 
        public void verMarca(String Marca) {
            System.out.println("Toyota");
        }
 
    };
 
}
 
class Automovil{
 
    public void vermarca(String Marca) {
 
        System.out.println("Chevrolet");
 
    }
 
}
 
interface Autos{
 
    public void Marcas(String Marca);
 
}
